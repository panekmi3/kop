#!/bin/bash

n=10

datasets=(
    "uf20-91"
    "ruf36-157"
    "ruf50-218"
    "ruf75-320")   


maxFlips=(
    "50000"
)

for dataset in ${datasets[@]}
do
    for filename in ./data/$dataset/*.cnf; 
    do
        for maxFlip in ${maxFlips[@]}
        do
            for (( i=1 ; i<=$n ; i++ ));
            do
                ./programs/gsat2 -r "$i" -p 0.4 -i "$maxFlip" "$filename" &
            done                    
        done                                
    done 2> "results_pilot/gsat/$dataset.csv"     > /dev/null         &

    for filename in ./data/$dataset/*.cnf; 
    do
        for maxFlip in ${maxFlips[@]}
        do
            for (( i=1 ; i<=$n ; i++ )); 
            do
                ./programs/probsat -r "$i" -i "$maxFlip" "$filename"      &
            done
        done                                                    
    done 2> "results_pilot/probsat/$dataset.csv"  > /dev/null         &
done

wait