class tableEntry:
    def __init__(self, threshold, probsat, gsat):
        self.threshold = threshold
        self.gsat = gsat
        self.probsat = probsat

thresholds = [ 100, 500, 1000, 5000, 10000, 20000, 30000, 40000, 50000]
datasets = ["uf20-91.csv","ruf36-157.csv","ruf50-218.csv","ruf75-320.csv"]
programs = ["gsat", "probsat"]

table = []

print("                gast    probsat")

for dataset in datasets:
    f_gsat = open("./results_pilot/gsat/" + dataset)
    f_prob = open("./results_pilot/probsat/" + dataset)
    gsat = []
    prob = []

    for line in f_gsat:
        gsat.append(int(line.split(" ")[0]))
    for line in f_prob:
        prob.append(int(line.split(" ")[0]))
    gsat.sort(reverse=True)
    prob.sort(reverse=True)

    print(dataset)
    for threshold in thresholds:
        smaller_than_threshold_gsat = len([element for element in gsat if element < threshold])/10000
        smaller_than_threshold_prob = len([element for element in prob if element < threshold])/10000
        print(f"{threshold:5}          {smaller_than_threshold_gsat:5}    {smaller_than_threshold_prob:5}")
