#include <stdlib.h>             /* strtol */
#include <stdio.h>              /* printf */
#include <string.h>             /* strcmp */
#include <math.h>               /* isnan etc. */
#ifdef _MSC_VER
#include "getopt.h"
#undef max
#undef min
#else
#include <unistd.h>             /* getopt */
#include <limits.h>
#endif
#include "rngctrl.h"
#include "num_opts.h"
/*-----------------------------------------------------------------------------*/
char synopsis[] = "picker <options> \n"
"\t Sample control\n"
"\t-k number                        sample size\n"
"\t-n number                        number of elements\n"
"\t-b number                        base to add to 0...n-1\n"
;
/*-----------------------------------------------------------------------------*/
#define I_NONE INT_MAX
/*-----------------------------------------------------------------------------*/
int main (int argc, char** argv) {
    /* parameters and default values*/
    int     k=I_NONE;   /* sample size */
    int     n=I_NONE;   /* number of elements */
    int     b=1;        /* base: output range b...n+b-1 */
    int     s=0;        /* generator state */
    int     c=0;
    int     done=0;
    int     i;
    double  p;
    int     nextint=0;  /* generated member */
    
    int     err=0;      /* err indicator */
    char    opt;        /* options scanning */

    /* ---------------------- options ----------------------- */
    while ((opt = getopt(argc, argv, "k:n:b:r:R:s:S:")) != -1) {
         switch (opt) {
         case 'k': k = par_double_rng (argv[0], opt, &err, 0.0, 1.0);     /* probability of random steps in an iteration */
                   break;
         case 'n': n = par_int (argv[0], opt, &err);  /* max clause length - needed when input from stdin */
                   break;
         case 'b': b = par_int (argv[0], opt, &err); /* max clause length - needed when input from stdin */
                   break;
         case 'r':      /* PRNG controls */
         case 'R': 
	     case 's':
         case 'S': if (!rng_options (opt, optarg, argv[0])) err++;
                   break;
         default:  fprintf (stderr, "%s%s", synopsis, rng_synopsis); 
                   return EXIT_FAILURE;  /* unknown parameter, e.g. -h */
         }
    }
    if (k == I_NONE) { fprintf (stderr, "%s: sample size (-k) missing\n", argv[0]); err++; }
    if (n == I_NONE) { fprintf (stderr, "%s: range (-n) missing\n", argv[0]); err++; }
    if (k >= n)      { fprintf (stderr, "%s: sample size must be less than range\n", argv[0]); err++; }
    if (err) return EXIT_FAILURE;                                   /* stop here if any error */

    /* ----------------- RNG controls ------------------------ */
    if (!rng_apply_options (argv[0])) return EXIT_FAILURE;          /* errors have been reported already */

    for (i=1; i<=k; i++) {
        done = 0;
        while (!done) {
            p = (double)(k-s)/(n-c);
            if (rng_next_double() < p) {
                nextint = c+b;
                s++;
                done = 1;
            }
            c++;
        }
        fprintf (stdout, "%d ", nextint);
    }
    fprintf (stdout, "\n");
    rng_end_options (argv[0]);
    return EXIT_SUCCESS;
}
