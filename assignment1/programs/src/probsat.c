#include <stdlib.h>             /* strtol */
#include <stdio.h>              /* printf */
#include <string.h>             /* strcmp */
#include <math.h>               /* isnan etc. */
#ifdef _MSC_VER
#include "getopt.h"
#include <windows.h>            /* ctrl c handler */
#undef max
#undef min
#else
#include <unistd.h>             /* getopt */
#include <limits.h>
#include <signal.h>
#endif
#include "ctrlc_handler.h"
#include "util_files.h"
#include "num_opts.h"
#include "sat_inst.h"
#include "sat_sol.h"
#include "rngctrl.h"
/*-----------------------------------------------------------------------------*/
char synopsis[] = "probsat <options> [dimacs-file]\n"
"\t Input format control\n"
"\t-w number                        max literals in a clause, default 3\n"
"\t Iteration control\n"
"\t-i number                        max iterations (flips)\n"
"\t-T number                        max tries (restarts)\n"
"\t Output control (iteration count and sat clauses to stdout)\n"
"\t-d <file>                        output iteration log into <file>\n"
"\t-t <file>                        detailed trace into <file>\n"
"\t-D                               debug info to stderr\n"
;

/*-----------------------------------------------------------------------------*/
/*      for all clauses in sol, update the number of true literals in cnt      */
/*-----------------------------------------------------------------------------*/
int pbs_eval (sol_t sol, inst_t* inst, cnt_t cnt) {
    int sat = 0, i, v;
    literal_t* clause;
    for (i=0, clause=inst->body; i<inst->length; i++, clause+=inst->width) {
        cnt[i] = 0;
        for(v=0; v<inst->width; v++) {
    	    if (clause[v] != 0) cnt[i]+=sol[clause[v]];
        }
        if (cnt[i] > 0) sat++;
    }
    return sat;
}
/*-----------------------------------------------------------------------------*/
/*      build the var_info structure telling where each variable is used       */
/*-----------------------------------------------------------------------------*/
var_info_t pbs_varinf_build (inst_t* inst) {
    var_info_t varinf;
    literal_t* clause;
    int i,v,l;
    varinf = calloc (inst->vars_no+1, sizeof(var_info));        /* item 0 is bogus */
    for (i=0, clause=inst->body; i<inst->length; i++, clause+=inst->width) {
        for(l=0; l<inst->width; l++) {
    	    if (clause[l] > 0) varinf[clause[l]].pos_occ_no++; else
    	    if (clause[l] < 0) varinf[-clause[l]].neg_occ_no++;
        }
    }
    for (v=1; v<=inst->vars_no; v++) {
        varinf[v].pos_occ = calloc (varinf[v].pos_occ_no, sizeof(clause_ix_t));
        varinf[v].pos_occ_no = 0;
        varinf[v].neg_occ = calloc (varinf[v].neg_occ_no, sizeof(clause_ix_t));
        varinf[v].neg_occ_no = 0;
    }
    for (i=0, clause=inst->body; i<inst->length; i++, clause+=inst->width) {
        for(l=0; l<inst->width; l++) {
    	    if (clause[l] > 0) {
    	        v=clause[l];
    	        varinf[v].pos_occ [varinf[v].pos_occ_no] = i; 
    	        varinf[v].pos_occ_no++; 
    	    } else
    	    if (clause[l] < 0) {
    	        v=-clause[l];
    	        varinf[v].neg_occ [varinf[v].neg_occ_no] = i; 
    	        varinf[v].neg_occ_no++; 
    	    }
        }
    }
    return varinf;
}
/*-----------------------------------------------------------------------------*/
/*      debug dump of the var_info structure to out                            */
/*-----------------------------------------------------------------------------*/
int pbs_varinf_dump (var_info_t varinf,inst_t* inst, FILE* out) {
    int i,v;
    for (v=1; v<=inst->vars_no; v++) {
        fprintf(out,"%3d P %3d:", v, varinf[v].pos_occ_no); 
        for (i=0; i<varinf[v].pos_occ_no; i++) fprintf(out," %3d",varinf[v].pos_occ[i]);
        fprintf(out,"\n");
        fprintf(out,"%3d N %3d:", v, varinf[v].neg_occ_no); 
        for (i=0; i<varinf[v].neg_occ_no; i++) fprintf(out," %3d",varinf[v].neg_occ[i]);
        fprintf(out,"\n");
    }
    return 0;
}

/*-----------------------------------------------------------------------------*/
/*      determine number of clauses broken with a variable flip                */
/*-----------------------------------------------------------------------------*/
int pbs_flip_nbreak (var_info_t varinf, sol_t sol, cnt_t cnt, int v) {
    int i, nbreak=0;
    if (sol[v]) {                                   /* var is 1, goes to 0 */
        for (i=0; i<varinf[v].pos_occ_no; i++) {    /* for all clauses where the variable occurs in a positive literal */
            if (cnt[varinf[v].pos_occ[i]] == 1 ) nbreak++; 
        }
    } else {                                        /* var is 0, goes to 1 */
        for (i=0; i<varinf[v].neg_occ_no; i++) {    /* for all clauses where the variable occurs in a negative literal */
            if (cnt[varinf[v].neg_occ[i]] == 1 ) nbreak++;
        }
    }
    return nbreak;
}
/*-----------------------------------------------------------------------------*/
/*      determine the change in satisfied clause number when variable v 0->1   */
/*-----------------------------------------------------------------------------*/
int pbs_flip_nmake (var_info_t varinf, sol_t sol, cnt_t cnt, int v) {
    int i, nmake=0;
    if (sol[v]) {                                   /* var is 1, goes to 0 */
        for (i=0; i<varinf[v].neg_occ_no; i++) {    /* for all clauses where the variable occurs in a negative literal */
            if (cnt[varinf[v].neg_occ[i]] == 0) nmake++;
        }
    } else {                                        /* var is 0, goes to 1 */
        for (i=0; i<varinf[v].pos_occ_no; i++) {    /* for all clauses where the variable occurs in a positive literal */
            if (cnt[varinf[v].pos_occ[i]] == 0) nmake++;
        }
    }
    return nmake;
}
/*-----------------------------------------------------------------------------*/
/*      realize flip 1->0 of variable v, update cnt                            */
/*-----------------------------------------------------------------------------*/
int pbs_make_neg_flip (var_info_t varinf, cnt_t cnt, int v) {
    int i, gain=0;
    for (i=0; i<varinf[v].pos_occ_no; i++) {    /* for all clauses where the variable occurs in a positive literal */
        if (cnt[varinf[v].pos_occ[i]] == 1) gain--;
        cnt[varinf[v].pos_occ[i]]--;
    }
    for (i=0; i<varinf[v].neg_occ_no; i++) {    /* for all clauses where the variable occurs in a negative literal */
        if (cnt[varinf[v].neg_occ[i]] == 0) gain++;
        cnt[varinf[v].neg_occ[i]]++;
    }
    return gain;
}
/*-----------------------------------------------------------------------------*/
/*      realize flip 0->1 of variable v, update cnt                            */
/*-----------------------------------------------------------------------------*/
int pbs_make_pos_flip (var_info_t varinf, cnt_t cnt, int v) {
    int i, gain=0;
    for (i=0; i<varinf[v].pos_occ_no; i++) {    /* for all clauses where the variable occurs in a positive literal */
        if (cnt[varinf[v].pos_occ[i]] == 0) gain++;     /* it is a waste to compute gain again */
        cnt[varinf[v].pos_occ[i]]++;
    }
    for (i=0; i<varinf[v].neg_occ_no; i++) {    /* for all clauses where the variable occurs in a negative literal */
        if (cnt[varinf[v].neg_occ[i]] == 1) gain--;
        cnt[varinf[v].neg_occ[i]]--;
    }
    return gain;
}
/*-----------------------------------------------------------------------------*/
/*      realize flip 1->0 of variable v, update cnt                            */
/*-----------------------------------------------------------------------------*/
int pbs_make_flip (var_info_t varinf, inst_t* inst, cnt_t cnt, sol_t sol, int v) {
    int gain=0;
    if (sol[v]) {
        gain += pbs_make_neg_flip (varinf, cnt, v);    /* update true literal counters */
    } else {
        gain += pbs_make_pos_flip (varinf, cnt, v);
    }
    sol_flip (sol, v);
    return gain;
}
/*-----------------------------------------------------------------------------*/
/*      randomly choose an unsatisfied clause                                  */
/*-----------------------------------------------------------------------------*/
int pbs_pick_unsat (inst_t* inst, cnt_t cnt, int satisfied) {
    unsigned c; int i,pick;
    c = rng_next_range(1, inst->length-satisfied);
    pick=0;
    for (i=0; i<inst->length; i++) {
        if (cnt[i] == 0) {
            pick++;
            if (pick == c) return i;
        }
    }
    return 0;
}
/*-----------------------------------------------------------------------------*/
/*      a heuristic function evaluating a variable                             */
/*-----------------------------------------------------------------------------*/

#define cm 0
#define cb 2.3f 

double pbs_f (int nmake, int nbreak) {
    return pow( nmake, cm ) / pow( nbreak + __DBL_EPSILON__, cb);
}
/*-----------------------------------------------------------------------------*/
/*      randomly choose a variable in a clause according to a heuristic        */
/*-----------------------------------------------------------------------------*/
int pbs_pick_var (var_info_t varinf, inst_t* inst, cnt_t cnt, sol_t sol, int cli) {
    static double* roulette=NULL;
    literal_t* clause;
    int w,i;
    int nmake, nbreak;
    double last, gain, pick;

    if (!roulette) roulette = calloc (inst->width, sizeof(double));
    if (!roulette) return 0;
    
    clause = inst->body+cli*inst->width;
    for (w=0; w<inst->width; w++) if (clause[w] == 0) break;    /* real clause width */
    last = 0.0;
    for (i=0; i<w; i++) {
        roulette[i] = last              /* options passing is missing below */
                    + pbs_f (pbs_flip_nmake (varinf, sol, cnt, abs(clause[i])), 
                             pbs_flip_nbreak (varinf, sol, cnt, abs(clause[i])));
        last = roulette[i];
    }
    /* now last is a sum of all pbs_f() */
    /* normalize the thresholds to 0...1 */
    for (i=0; i<w; i++) {
        roulette[i] = roulette[i] / last;
    }
    pick = rng_next_double();
    for (i=0; i<w; i++) {
        if (pick <= roulette[i]) break;  
    }
    return (clause[i] < 0) ? -clause[i] : clause[i];
}
/*-----------------------------------------------------------------------------*/
int main (int argc, char** argv) {
    /* parameters and default values*/
    int     width=3;
    int     itrmax=300; /* max iterations */
    int     triesmax=1; /* max tries */
    int     debug=0;    /* debug info to stderr */
    file_t  in =    {NULL, stdin};  /* instance input */
    file_t  data =  {NULL, NULL};   /* evolution records, outsep applies */
    file_t  trace = {NULL, NULL};   /* detailed trace */

    int     err=0;      /* err indicator */
    char opt;           /* options scanning */
    inst_t  inst;       /* instance */
    sol_t   sol;        /* solution */
    cnt_t   cnt;        /* true literals counters, per clause */
    int     satisfied;  /* current no. of sat clauses */ 
    int     flipvar;    /* max gain or picked flipping variable */
    int     ucli;       /* picked unsat clause */
    int     gain;       /* flip gain */
    var_info_t varinf;  /* inverted instance */
    int     itrno;      /* iteration number within a try */
    int     tryno;	    /* number of restarts */
    /* --------------------- CTRL-C handling ---------------- */    
    int*    pcont = establish_handler(argv[0]);
    if (!pcont) return EXIT_FAILURE;   

    /* ---------------------- options ----------------------- */
    while ((opt = getopt(argc, argv, "T:t:d:Di:w:r:R:s:S:")) != -1) {
         switch (opt) {
         case 'd': data.name = optarg; break;    /* datafile required */
         case 't': trace.name = optarg; break;   /* trace required */
         case 'D': debug=1; break;              /* debugging required */
         case 'w': width = par_int_min (argv[0], opt, &err, 1);    /* max clause length - needed when input from stdin */
                   break;
         case 'i': itrmax = par_int_min (argv[0], opt, &err, 0);   /* max no. of iterations - 0 means no limit */
                   break;
         case 'T': triesmax = par_int_min (argv[0], opt, &err, 0); /* max no. of tries - 0 means no limit */
                   break;
         case 'r':      /* PRNG controls */
         case 'R': 
	     case 's':
         case 'S': if (rng_options (opt, optarg, argv[0])) err++;
                   break;
         default:  fprintf (stderr, "%s%s", synopsis, rng_synopsis); 
                   return EXIT_FAILURE;  /* unknown parameter, e.g. -h */
         }
    }
    if (optind < argc) in.name = argv[optind];                      /* input file on the command line */
    if (err) return EXIT_FAILURE;                                   /* stop here if any error */

    /* ----------------- RNG controls ------------------------ */
    if (!rng_apply_options (argv[0])) return EXIT_FAILURE;          /* errors are reported already */
    
    /* ----------------------- instance input ---------------- */
    if (! util_file_in (&in)) return EXIT_FAILURE;
    if (in.name) {
        if ((width = inst_width(&inst, in.file)) < 0) {
            inst_read_fail (width, argv[0]);
            return EXIT_FAILURE;
        }
        rewind(in.file);    
    }
    err = inst_read(&inst, in.file, width);                        /* if any error so far, report and exit */
    if (err) {
        inst_read_fail (err, argv[0]);
        return EXIT_FAILURE;
    }
    
    /* ------------------------ datafile output -------------- */   
    if (! util_file_log (&data)) return EXIT_FAILURE;    

    /* ------------------------ tracefile output ------------- */   
    if (! util_file_log (&trace)) return EXIT_FAILURE;    

    /* ----------------------- instance inversion ------------- */
    if (!(varinf = pbs_varinf_build (&inst))) {                      /* build the 'where used' structure */
        fprintf (stderr, "%s: allocation failure\n", argv[0]); return EXIT_FAILURE;
    }	
    if (debug) pbs_varinf_dump (varinf, &inst, stderr);

    /* ----------------------- solution  space  --------------- */
    if (!(sol = sol_reserve(inst.vars_no))) {                       /* build the solution arrray  */
        fprintf (stderr, "%s: allocation failure\n", argv[0]); return EXIT_FAILURE;
    }	

    tryno = 1;
    itrno = 0; 
    satisfied = 0;
    while (satisfied < inst.length && *pcont && ((!triesmax) || tryno <= triesmax)) {

        sol_rand (sol, inst.vars_no);                                   /* random 0/1 assignment */
        /* ----------------------- evaluation --------------------- */
        if (!(cnt = cnt_reserve(inst.length))) {                        /* build the array of true literal counts */
            fprintf (stderr, "%s: allocation failure\n", argv[0]); return EXIT_FAILURE;
        }	
        satisfied = pbs_eval (sol, &inst, cnt);                          /* evaluate true literals and count sat clauses */
    
        /* ----------------------- debug and trace ---------------- */
        if (data.file) fprintf (data.file, "%d %d\n", 0, satisfied);
        if (debug) {
            sol_write(sol, stderr, inst.vars_no); 
            fprintf(stderr,"satisfied: %d\n",satisfied);
            for (int i=0; i<inst.length; i++) fprintf(stderr, " %d", cnt[i]); 
            fprintf(stderr, "\n");
        }
        if (trace.file) { 
            fprintf (trace.file, "initial: satisfied %d, solution: ", satisfied);
            sol_write(sol, trace.file,  inst.vars_no); 
            fprintf (trace.file, "true literals: ");
            for (int i=0; i<inst.length; i++) fprintf(trace.file, " %d", cnt[i]); 
            fprintf(trace.file, "\n");
        }
        /* ----------------------- gsat inner iteration ----------- */
        itrno = 1; gain=1;                                              /* stop when formula satisfied, CTRL-C occurs */
                                                                    /* and then either iterations unlimited or still below limit */
        while (satisfied < inst.length && *pcont && ((!itrmax) || itrno <= itrmax)) {
            ucli = pbs_pick_unsat (&inst, cnt, satisfied);           /* pick some unsat clause at random */
            flipvar = pbs_pick_var (varinf, &inst, cnt, sol, ucli);  /* pick a variable in that clause */
            gain = pbs_make_flip (varinf, &inst, cnt, sol, flipvar); /* update the true literals counters, determine gain */

            satisfied += gain;                                        /* update sat clauses no. */
            if (data.file) fprintf (data.file, "%d %d\n", itrno, satisfied);      /* datafile line */
            if (debug) {                                              /* debug info */
                fprintf(stderr,"flipvar %d, satisfied: %d\n", flipvar, satisfied);
            }
            if (trace.file) {                                         /* readable trace info */
                fprintf (trace.file, "itr %d, flipvar %d, satisfied %d, solution: ", itrno, flipvar, satisfied);
                sol_write(sol, trace.file,  inst.vars_no); 
                fprintf (trace.file, "true literals: ");
                for (int i=0; i<inst.length; i++) fprintf(trace.file, " %d", cnt[i]); 
                fprintf(trace.file, "\n");
            }
            itrno++;
        }
        tryno++;
    }
    fprintf (stderr, "%d %d %d %d\n", (tryno-2)*itrmax+itrno-1, triesmax*itrmax, satisfied, inst.length);    /* final information */
    sol_write (sol, stdout, inst.vars_no);
    rng_end_options (argv[0]);
    util_file_close(&data);
    util_file_close(&trace);

    return EXIT_SUCCESS;
}
