import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st

datasets = ["uf20-91","ruf36-157","ruf50-218","ruf75-320"]
programs = ["gsat", "probsat"]

# datasets = ["ruf75-320"]
# programs = ["gsat", "probsat"]

colors={ "gsat":"orange","probsat":"darkcyan"}

def histograms():
    for dataset in datasets:
        plt.figure()
        
        for program in programs:
            file = open(f"./results/{program}/{dataset}.csv")
            file_data = file.read().split('\n')
            data = []
            for line in file_data:
                final, max_flips, clauses, satisfied = map(int, line.split(' '))
                data.append(final)
                
            plt.hist(data, bins=20, label=program, color=colors[program], histtype='barstacked', rwidth=0.9, edgecolor='black', alpha=0.5)
            
        plt.title(dataset)
        plt.legend()
        plt.xticks(list(range(0, max_flips, int(max_flips/10))).append(max_flips))
        plt.savefig(f"./images/hist_{dataset}.png")

def ecdfs():
    for dataset in datasets:
        plt.figure()
        
        for program in programs:
            file = open(f"./results/{program}/{dataset}.csv")
            file_data = file.read().split('\n')
            data = []
            for line in file_data:
                final, max_flips, clauses, satisfied = map(int, line.split(' '))
                if clauses==satisfied: data.append(final)
                
            ax=plt.subplot()
            ax.set_ylim(ymin=0, ymax=1)
            ax.set_xlim(xmin=0, xmax=max_flips)
            st.ecdf(data).cdf.plot(label=program, color=colors[program])
        plt.title(dataset)
        plt.legend()
        plt.savefig(f"./images/ecdf_{dataset}.png")

def main():
    histograms()
    ecdfs()
    
if __name__ == "__main__":
    main()