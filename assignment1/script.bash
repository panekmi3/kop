#!/bin/bash

n=1000

datasets=(
    "uf20-91"
    "ruf36-157"
    "ruf50-218"
    "ruf75-320"
    )

maxFlips=0

for dataset in ${datasets[@]}
do

    if [[ maxFlips -eq 10000 ]] 
    then 
        maxFlips=30000 
    fi
    
    if [[ maxFlips -eq 5000 ]]
    then 
        maxFlips=10000 
    fi
    if [[ maxFlips -eq 1000 ]]
    then 
        maxFlips=5000
    fi
    if [[ maxFlips -eq 0    ]]
    then
        maxFlips=1000
    fi
    echo "$maxFlips"

    for filename in ./data/$dataset/*.cnf; 
    do
        for (( i=1 ; i<=$n ; i++ ));
        do
            ./programs/gsat2 -r "$i" -p 0.4 -i "$maxFlips" "$filename" &
        done                                                    
    done 2> "results/gsat/$dataset.csv"     > /dev/null         &

    for filename in ./data/$dataset/*.cnf; 
    do
        for (( i=1 ; i<=$n ; i++ )); 
        do
            ./programs/probsat -r "$i" -i "$maxFlips" "$filename"      &
        done                                                    
    done 2> "results/probsat/$dataset.csv"  > /dev/null         &
done

wait
for filename in ./results/gsat/*.csv; 
do
    wc "$filename"
done
for filename in ./results/probsat/*.csv; 
do
    wc "$filename"
done
