import numpy as np
import matplotlib.pyplot as plt
import os

def compile(constants):
    os.system(f"g++ src/simulatedCooling.cpp -std=c++20 -O4 -o simulatedannealing -DWHITEBOX -DEQ={constants[0]} -DIT={constants[1]} -DET={constants[2]} -DCR={constants[3]} -DCC={constants[4]}" )

def run(dataset):
    #print(f"./simulatedannealing ./data/{dataset} 2> ./results/whitebox/temp_file.csv 1> /dev/null")
    os.system(f"./simulatedannealing ./data/{dataset} 2> ./results/whitebox/temp_file.csv 1> /dev/null")

def graph():
    file = open("./results/whitebox/temp_file.csv")
    first_line = file.readline().split(' ')
    directory = os.path.basename(os.path.dirname(first_line[0])) 
    dataset = os.path.basename(first_line[0])
    dataset = dataset.split('.')[0]
    MAX = int(first_line[1])
    EQ = int(first_line[2])
    IT = float(first_line[3])
    ET = float(first_line[4])
    CR = float(first_line[5])
    CC = float(first_line[6])
    
    lines = file.readlines()
    data=[]
    
    for line in lines:
        line_split  = line.split(' ')
        satisfied   = int(line_split[0])
        weight      = float(line_split[1])
        cost        = float(line_split[2])
        data.append((satisfied/MAX, weight, cost))
        
    plt.figure()
    plt.plot( range(0, len(data)), [tup[2] for tup in data], label="cost" )
    plt.plot( range(0, len(data)), [tup[0] for tup in data], label="satisfied %")
    #plt.ylim((0.8, 1.05))
    plt.xlabel("Iteration")
    plt.legend()
    plt.suptitle(dataset)
    plt.title( f"EQ={EQ}, IT={IT}, ET={ET}, CR={CR}, CC={CC}" )
    plt.savefig(f"./results/whitebox/{directory}/{dataset}_{EQ}_{IT}_{ET}_{CR}_{CC}.png")
    plt.close()
    
def main():
    datasets=[
    "wuf20-91-M/wuf20-01.mwcnf",
    "wuf20-91-R/wuf20-01.mwcnf",
    "wuf50-218-M/wuf50-01.mwcnf",
    "wuf50-218-R/wuf50-01.mwcnf",
    "wuf75-325-M/wuf75-01.mwcnf",
    "wuf75-325-R/wuf75-01.mwcnf"
    ]

    constants ={
        0: 50,
        1: 1,
        2: 0.001,
        3: 0.99,
        4: 0.001
    }

    base_constants ={
        0: 50,
        1: 1,
        2: 0.001,
        3: 0.99,
        4: 0.001
    }

    EQ_array = [10, 20, 40, 80, 160]
    IT_array = [10, 5, 1, 0.5, 0.2]
    ET_array = [0.1, 0.01, 0.001, 0.0001, 0.00001]
    CR_array = [0.999, 0.99, 0.98, 0.95, 0.90]
    CC_array = [0.1, 0.01, 0.001, 0.0001, 0.00001]

    constants_array = [EQ_array, IT_array, ET_array, CR_array, CC_array]
    
    for i in range(len(constants_array)):
        for j in range(len(constants_array[i])):
            constants[i] = constants_array[i][j]       
            compile(constants)
            for dataset in datasets:    
                run(dataset)
                graph()
        constants[i] = base_constants[i]
    
    
if __name__ == "__main__":
    main()