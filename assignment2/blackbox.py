import numpy as np
import matplotlib.pyplot as plt
import os

datasets=[
    "wuf20-91-M",
    "wuf20-91-N",
    "wuf20-91-Q",
    "wuf20-91-R",
    "wuf50-218-M",
    "wuf50-218-N",
    "wuf50-218-Q",
    "wuf50-218-R",
    "wuf75-325-M",
    "wuf75-325-N",
    "wuf75-325-Q",
    "wuf75-325-R"
    ]

def main():
    
    print("dataset SC SCP MW MWP TL")
    for dataset in datasets:
        results_file = open(f"./results/blackbox/{dataset}.csv")
        template_file = open(f"./data/{dataset}-opt.dat")
        sat_file = open(f"./results/blackbox/sat_{dataset}.csv")
        
        results_lines = results_file.readlines()
        template_lines = template_file.readlines()
        sat_lines = sat_file.readlines()
                
        if len(results_lines) != len(template_lines): print("ERROR")
        
        results_lines.sort()
        template_lines.sort()
        
        count = 0
        unsat = 0
        target = int(dataset.split('-')[1])
        
        for i in range(len(results_lines)):
            if results_lines[i] != template_lines[i]: count=count + 1
            if int(sat_lines[i]) != target: unsat = unsat + 1
        
        TL  = len(results_lines)
        SC  = TL - unsat
        SCP = (SC/TL)*100
        MW  = TL - count
        MWP = (MW/TL)*100
        
        print(f"{dataset}: {SC} {SCP} % {MW} {MWP} % {TL}")
        
            


if __name__ == "__main__":
    main()