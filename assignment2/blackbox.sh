#!/bin/bash


datasets=(
    "wuf20-91-M"
    "wuf20-91-N"
    "wuf20-91-Q"
    "wuf20-91-R"
    # "wuf50-218-M"
    # "wuf50-218-N"
    # "wuf50-218-Q"
    # "wuf50-218-R"
    # "wuf75-325-M"
    # "wuf75-325-N"
    # "wuf75-325-Q"
    # "wuf75-325-R"
)

g++ src/simulatedCooling.cpp -std=c++20 -O4 -o simulatedannealing

for dataset in ${datasets[@]}
do  
    echo "$dataset"
    for filename in ./data/$dataset/*.mwcnf; 
    do
        ./simulatedannealing "$filename" &
    done > "./results/blackbox/$dataset.csv" 2> "./results/blackbox/sat_$dataset.csv"
done

wait