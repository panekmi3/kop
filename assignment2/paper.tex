\documentclass{article}
\usepackage{graphicx} % Required for inserting images
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{capt-of}
\usepackage[a4paper, total={7in, 8in}]{geometry}
\usepackage{minted} 


\title{Aplikace pokročilé heuristiky při řešení problému vážené splnitelnosti Boolevých formulí}
\author{Miloš Pánek}
\date{Febuary 2024}

\begin{document}

\maketitle

\tableofcontents

\section{Shrnutí}
Práce se zabývá návrhem a testováním algoritmu simulovaného žíhání pro řešení optimalizačního problému řešení splnitelnosti vážené Booleovy formule s maximální váhou. Práce popisuje princip činnosti algoritmu, postup při určení parametrů pro jeho nejlepší fungování pomocí white box testování a důkladné black box testování. Výsledkem práce je algoritmus schopný uspokojivě řešit instance problému wuf20-91 a wuf 50-218. Pro problémy wuf-75-325 algoritmus nachází platné ohodnocení, ovšem nedosahuje maximální možné váhy.

\section{Zadání}
Přesné znění zadání je dostupné na courses stránce předmětu.\cite{zadani}
\subsection{Popis problému}
 
Je dán vektor X boolovských proměnných \begin{math}(x_1, ..., x_n) \end{math}, vektor \begin{math}(w_1, ..., w_n) \end{math} a Booleova formule v konjunktivním normálním tvaru. úkolem je najít takový vektor X, aby ohodnocení formule bylo platné a zároveň aby \begin{math} \sum_{i=1}^{n} x_i*w_i \end{math} byla maximální. Pro účly úlohy pracujeme s 3SAT verzí problému, každá klauzule formule má tak právě tři literály.

\section{Algoritmus}
Pro řešení úlohy byl zvolen algoritmus simulovaného žíhání implementovaný v jazyce C++ za pomoci knihovny boost\cite{boost}.
\subsection{Popis algoritmu SA}

Algoritmus napodobuje fyzikální princip tuhnutí taveniny. Uchovává si aktuální teplotu a pracuje v iteracích, ve kterých zkouší nové stavy ze stavového prostoru problému. Tyto stavy charakterizuje pomocí jejich ceny, která udává, jak moc stav vyhovuje řešení problému. Pokud algoritmus najde stav, který je lepší nežli stav aktuální, přejde do tohoto stavu. Pokud stav není lepší než aktuální, může do tohoto stavu stále přejít. Pravděpodobnost tohoto přechodu se snižuje s rozdílem ceny stavů a snižující se teplotou. Algoritmus takto plynule přechází mezi fází diversifikace, kdy mu vysoká teplota dovoluje prohledat co největší část stavového prostoru a fází intensifikace, kdy jsou díky nízké teplotě přijímány pouze stavy s vyšší cenou.Teplotu algoritmus postupně snižuje dokud nedosáhne teploty minimální. Během běhu si algoritmus uchovává nejlepší nalezený stav, který po skončení vrátí jako řešení.

\subsection{Implementace}
 Algoritmus pracuje ve dvou smyčkách. Vnější smyčka, běžící dokud není dosaženo minimální teploty a smyčka vnitřní, během které jsou zkoušeny nové stavy. Implementace odpovídá pseudokódu z přednášek předmětu NI-KOP\cite{pred}.
 \begin{minted}{c++}

    while ( temperature > endTemp )
    {
        while(equilibrium)
        {
            state = try_state(state, temperature);

            if( state->cost() > bestState->cost() )
            {
                bestState = state;
                if(state->solution())
                    bestSolved = state;             
            }

            equilibrium--;        
        }
        equilibrium = EQ;
        temperature = cool(temperature);
        
    }
 
\end{minted}

\subsubsection{Výběr sousedního stavu}
V každé iteraci algoritmu je volána funkce try\_state. Ta nalezne nějakého souseda aktuálního stavu a na základě jeho ceny a teploty jej vrátí.

 \begin{minted}{c++}

shared_ptr<CState> try_state( shared_ptr<CState> state, double temperature)
{
    shared_ptr<CState> neighbor = state->getRandomNeighbor();
    if( neighbor->cost() > state->cost() ) return neighbor;

    double diff = state->cost() - neighbor->cost();

    if( unif(re) < exp( -diff/temperature) )
        return neighbor;
    return state;
}
 
\end{minted}

Třída CState představuje jeden ze stavů problému. Uchovává pravdivostní ohodnocení proměnných a poskytuje rozhraní pro výpočet hodnoty tohoto stavu. Také poskytuje metodu, která vrací nějakého souseda aktuálního stavu. Pokud je ohodnocení formule v daném stavu pravdivé, metoda vrátí stav odvozený od aktuálního negací náhodné proměnné. Pokud však aktuální stav není splněný, je negována proměnná vyskytující se v náhodné nesplněné klauzuli. Tím je algoritmus tlačen k platným ohodnocením formule.

\begin{minted}{c++}
std::shared_ptr<CState> CState::getRandomNeighbor() const
{
    small_vector<bool, 80> newValues = values;
    if(solution())
    {
        int index = rand() % newValues.size();
        newValues[index] = ! newValues[index]; 
    }
    else
    {
        small_vector<CClause, 325> unsat_clauses;
        for (auto clause : instance.getClauses())
        {
            if ( ! clause.eval(values))
            {
                unsat_clauses.push_back(clause);
            }
        } 

        assert(unsat_clauses.size());

        int index = rand() % unsat_clauses.size();
        CClause clause = unsat_clauses[index];
        small_vector<CLiteral, 3> literals = clause.getLiterals();
        index = rand() % literals.size();
        CLiteral literal = literals[index];
        newValues[literal.getIndex()] = ! newValues[literal.getIndex()];
    }
    return std::make_shared<CState>(newValues, instance);
}
\end{minted}

\subsubsection{Cena stavu}
Cena stavu je určena pomocí metody cost. Cenu ovlivňuje součet vah stavu, normalizovaný pomocí nejvyšší váhy v instanci a poměr splněných klauzulí ku celkovému počtu klauzulí. Výsledná cena je poté rovna \begin{math}const = sum * CC + (1-CC)*sat\end{math}. CC zde představuje parametr koeficient ceny. Tento koeficient určuje, kterou ze složek ceny algoritmus preferuje.
\begin{minted}{c++}
double CState::cost() const
{
    double normalWeigt = (weightedSum * 1.0) / instance.getLargestWeight();
    double normalSat = ( satisfied * 1.0 ) / instance.getClauses().size();

    return CC * normalWeigt + (1-CC) * normalSat;
}
\end{minted}

\subsubsection{Parametry algoritmu}
\begin{description}
\item[Ekvilibrium (EQ)]: Udává počet vnitřních iterací algoritmu. Během těchto iterací není upravována teplota. Vyšší hodnoty umožňují algoritmu prozkoumat více stavů ale zvyšují dobu běhu.

\item[Počáteční teplota  (IT)]: Udává počáteční teplotu. Vyšší hodnota umožňuje algoritmu prozkoumat větší část stavového prostoru, protože stráví delší čas s vyšší teplotou, kdy je pravděpodobnější, že přejde do méně výhodného stavu. Vysoká hodnota prodlužuje dobu běhu algoritmu.

\item[Koncová teplota (ET)]: Udává teplotu, při které se má algoritmus zastavit. Vysoká hodnota neumožní programu dosáhnout ustáleného bodu. Příliš nízká hodnota prodlužuje dobu běhu a to často zbytečně, protože při nízké teplotě je malá pravděpodobnost, že algoritmus změní stav do méně výhodného stavu a je tak nízká pravděpodobnost, že by opustil případný lokální extrém.

\item[Koeficient chlazení (CR)]: Udává rychlost snižování teploty. Teplota v kroku n je rovna \begin{math} T_n = T_{n-1}*CR\end{math}. Koeficient musí být nižší než jedna, aby docházelo k snižování teploty. Vysoké hodnoty prodlužují dobu běhu, dovolují ovšem algoritmu prozkoumat větší část stavového prostoru, protože mu dovolí strávit delší dobu s vysokou teplotou.

\item[Koeficient ceny (CC)]: Udává preferenci algoritmu pro stavy s vysokou váhou nebo pro stavy s vysokým počtem kladně ohodnocených klauzulí. Čím je nižší tím algoritmus tíhne ke stavům, kdy je pravdivě ohodnocen co největší počet klauzulí.
\end{description}

\subsubsection{Optimalizace}
Program namísto standartních kontejnerů std::vector používá kontejnery small\_vector z knihovny boost. Small\_vector má stejné rozhraní i chování jako standartní vektory, implementuje ovšem SBO (Small Buffer Optimization). Ve zkratce má statické úložiště kde může uchovávat data bez potřeby úložiště alokovat dynamicky. Dochází tak k výrazné úspoře času. Velikost úložiště však musí být známá v době kompilace, vektory jsou proto dimenzovány pro největší z instancí. Při zpracování malých instancí tak dochází k plýtvání paměti, rychlostní přínos je však zásadní. Zrychlení nebylo pečlivě testováno, ovšem rychlé testy odhalili přibližně osminásobné zrychlení oproti použití standardních kontejnerů.

\section{White box fáze}
Cílem white box fáze návrhu je určení vhodných hodnot pro parametry programu. nalezení vhodných hodnot ne dosaženo testováním algoritmu pro různé hodnoty parametrů a sledováním chovaání algoritmu při těchto hodnotách.
\begin{description}
\item[Testované hodnoty parametrů]
\item[EQ]: 10, 20, 40, 80, 160
\item[IT]: 10, 5, 1, 0.5, 0.2
\item[ET]: 0.1, 0.01, 0.001, 0.0001, 0.00001
\item[CR]: 0.999, 0.99, 0.98, 0.95, 0.90
\item[CC]: 0.1, 0.01, 0.001, 0.0001, 0.00001
\end{description}
Při testování byl vždy měněn pouze jeden z parametrů, aby byl zřejmý jeho dopad na chování algoritmu. Ostatní parametry byly fixní s hodnotami: EQ=50, IT=1, ET=0.001, CR=0.99 a CC=0.001. Jedná se o hodnoty, které byly odvozeny během testování algoritmu v průběhu vývoje a poskytovali uspokojivé výsledky pro účely vývoje.

Při white box fázi byl algoritmus spuštěn na první instanci datových sad wuf20-91, wuf50-218 a wuf75-325 \cite{data} ve variantách M a R. Program byl upraven, aby v každé fázi vypsal počet splněných klauzulí spolu s cenou stavu. V následující sekci jsou k nahlédnutí grafy zachycující vývoj těchto hodnot pro různé hodnoty parametrů. V dokumentu je obsažen pouze výběr těchto grafů pro datovou sadu wuf50-218-R, další jsou k nahlédnutí v přiloženém archivu.

\subsection{Ekvilibrium}
Výsledná hodnota ekvilibria byla zvolena 80. Nižší hodnoty neposkytují algoritmu dostatečný čas pro prohledání stavového prostoru. Vyšší hodnoty prodlužují dobu běhu bez očividného přínosu.

\begin{figure}[h!]
\noindent
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_10_1.0_0.001_0.99_0.001.png}
        \label{fig:h_uf20-91}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_40_1.0_0.001_0.99_0.001.png}
        \label{fig:h_ruf36-157}
    \end{subfigure}%
    \newline
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_80_1.0_0.001_0.99_0.001.png}
        \label{fig:h_ruf50-218}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_160_1.0_0.001_0.99_0.001.png}
        \label{fig:h_ruf75-320}
    \end{subfigure}%

    \caption{Vývoj ceny stavu a procenta splněných klauzulí pro různé hodnoty EQ}
    \label{fig:EQ}
\end{figure}

\subsubsection{Počáteční teplota}
Pro počáteční hodnotu byla zvolena hodnota 1. Vyšší hodnoty zvyšovali dobu běhu programu bez viditelného přínosu.
\begin{figure}[h!]
\noindent
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_0.2_0.001_0.99_0.001.png}
        \label{fig:h_uf20-91}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.99_0.001.png}
        \label{fig:h_ruf36-157}
    \end{subfigure}%
    \newline
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_5.0_0.001_0.99_0.001.png}
        \label{fig:h_ruf50-218}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_10.0_0.001_0.99_0.001.png}
        \label{fig:h_ruf75-320}
    \end{subfigure}%

    \caption{Vývoj ceny stavu a procenta splněných klauzulí pro různé hodnoty IT}
    \label{fig:EQ}
\end{figure}

\subsection{Koncová teplota}

Ze čtvrtého z grafů je zřejmé, že ideální hodnota bude vyšší, nežli 0.0001. V tomto případě algoritmus strávil dlouhou dobu poté, co byla hodnota stavu ustálena, výpočet tak nevedl ke zlepšení. Ovšem pro nižší testovanou hodnotu 0.001 došlo k ustálení krátce před dosažením minimální teploty. Pro parametr byla zvolena hodnota 0.0005 jako kompromis mezi zmíněnými hodnotami.

\begin{figure}[h!]
\noindent
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.1_0.99_0.001.png}
        \label{fig:h_uf20-91}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.01_0.99_0.001.png}
        \label{fig:h_ruf36-157}
    \end{subfigure}%
    \newline
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.99_0.001.png}
        \label{fig:h_ruf50-218}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.0001_0.99_0.001.png}
        \label{fig:h_ruf75-320}
    \end{subfigure}%

    \caption{Vývoj ceny stavu a procenta splněných klauzulí pro různé hodnoty ET}
    \label{fig:EQ}
\end{figure}


\subsection{Koeficient ochlazování}
Hodnota 0.99 nabízí nejlepší poměr mezi délkou běhu a poskytovanými výsledky a délkou běhu. Parametr má zásadní vliv na dobu běhu, protože určuje rychlost ochlazování. Pro nízké hodnoty však dochází k rychlému zamrznutí, algoritmus tak nemá možnost prohledat dostatek stavového prostoru.

\begin{figure}[h!]
\noindent
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.9_0.001.png}
        \label{fig:h_uf20-91}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.95_0.001.png}
        \label{fig:h_ruf36-157}
    \end{subfigure}%
    \newline
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.98_0.001.png}
        \label{fig:h_ruf50-218}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.999_0.001.png}
        \label{fig:h_ruf75-320}
    \end{subfigure}%

    \caption{Vývoj ceny stavu a procenta splněných klauzulí pro různé hodnoty CR}
    \label{fig:CR}
\end{figure}

\subsection{Koeficient ceny}
Pro vysoké hodnoty algoritmus preferuje stavy poskytující nejvyšší vážený součet proměnných na úkor počtu splněných klauzulí. To je patrné z prvního grafu s hodnotou 0.1, ze kterého je vidět jak s rostoucí cenou stavu klesá procento splněných klauzulí. Opačný extrém představuje poslední z grafů, kdy křivky obou metrik takřka splývají v jednu, na cenu stavu má tak vliv pouze počet splněných klauzulí a o váhy se algoritmus příliš nezajímá. Nakonec byla zvolena hodnota 0.001, při které algoritmus tíhne spíše ke stavům s vyšším počtem splněných klauzulí.

\begin{figure}[h!]
\noindent
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.99_0.1.png}
        \label{fig:h_uf20-91}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.99_0.01.png}
        \label{fig:h_ruf36-157}
    \end{subfigure}%
    \newline
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.99_0.001.png}
        \label{fig:h_ruf50-218}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=1\linewidth]{graphics/wuf50-01_50_1.0_0.001_0.99_1e-05.png}
        \label{fig:h_ruf75-320}
    \end{subfigure}%

    \caption{Vývoj ceny stavu a procenta splněných klauzulí pro různé hodnoty CC}
    \label{fig:CC}
\end{figure}

\subsubsection{Shrnutí zvolených hodnot parametrů}
Podle výsledků white box fáze byly zvoleny následujíví hodnoty:
\begin{description}
    \item[EQ]: 80
    \item[IT]: 1.0
    \item[ET]: 0.0005
    \item[CR]: 0.99
    \item[CC]: 0.001
\end{description}





\section{Black box fáze}
Cílem black box fáze je ověření chování algoritmu na větším počtu instancí daného problému. Algoritmus byl spouštěn na poskytnutých sadách wuf20-91, wuf50-218 a wuf75-325 ve všech jejich variantách (M, N, Q a R). Výstup programu by upraven aby odpovídal formátu souborů s optimálními výsledky, které jsou k sadám přiloženy. Program také na chybový výstup vypíše počet splněných klauzulí přijatého stavu, aby bylo možné odvodit počet splněných instancí ve kterých nebylo dosaženo maximální váhy.

\subsection{Výsledky}
Výsledky testovávaní jsou uvedeny v tabulce \ref{tab:my-table}.


\begin{description}
    \item[Vysvětlivky k hodnotám v tabulce výsledků]
    \item[dataset]: uvádí datovou sadu ze které pochází testované instance
    \item[SI]: Satisfied Instances, udává počet instancí kdy algoritmus dosáhl platného ohodnocení formule
    \item[SIP]: Satisfied Instances Percentage, udává procento instancí kdy algoritmus dosáhl platného ohodnocení formule
    \item[MW]: Maximum Weight, udává počet instancí kdy algoritmus dosáhl maximální váhy
    \item[MWP]: Maximum Weight Percentage, udává procento instancí kdy algoritmus dosáhl maximální váhy
    \item[AT]: Average time, udává průměrný čas pro výpočet jedné instance\footnote{Číslo je orientační, program byl pro 
    jednotlivé instance spouštěn paralelně, vždy pro celý dataset. Průměrný čas je určen jako user time naměřený unix utilitou time děleno počtem instancí. Při běhu jednoho programu samostatně byl čas běhu typicky nižší}
    \item[TL]: Total, celkový počet instancí v datasetu. 
\end{description}

\begin{table}[!h]
\begin{tabular}{|l||l|l|l|l|l|l|}
\hline
dataset      & SI   & SIP {[}\%{]} & MW  & MWP {[}\%{]} & AT {[}s{]} & TL   \\ \hline
wuf20-91-M:  & 1000 & 100.0        & 970 & 97.0         & 0.9        & 1000 \\ \hline
wuf20-91-N:  & 1000 & 100.0        & 973 & 97.3         & 0.9        & 1000 \\ \hline
wuf20-91-Q:  & 1000 & 100.0        & 973 & 97.3         & 0.9        & 1000 \\ \hline
wuf20-91-R:  & 1000 & 100.0        & 970 & 97.0         & 0.9        & 1000 \\ \hline
wuf50-218-M: & 992  & 99.2         & 829 & 82.89        & 2.1       & 1000 \\ \hline
wuf50-218-N: & 993  & 99.3         & 830 & 83.0         & 2.1       & 1000 \\ \hline
wuf50-218-Q: & 987  & 98.7         & 736 & 73.6         & 2.1       & 1000 \\ \hline
wuf50-218-R: & 985  & 98.5         & 697 & 69.69        & 2.1       & 1000 \\ \hline
wuf75-325-M: & 91   & 91.0         & 40  & 40.0         & 3.5       & 100  \\ \hline
wuf75-325-N: & 91   & 91.0         & 40  & 40.0         & 3.5       & 100  \\ \hline
wuf75-325-Q: & 84   & 84.0         & 28  & 28.00        & 3.5       & 100  \\ \hline
wuf75-325-R: & 88   & 88.0         & 28  & 28.00        & 3.5       & 100  \\ \hline
\end{tabular}
\caption{Výsledky black box testování}
\label{tab:my-table}
\end{table}

\section{Závěr}

Výsledkem práce je algoritmus pro řešení problému vážené Booleovské formule. Algoritmus je schopný řešit instance problému z datové sady wuf20-91 s takřka stoprocentní úspěšností a to i pro sady označené jako zavádějící. Pro sadu wuf50-218 algoritmus nachází platné ohodnocení formule ve více než 98 procentech případů. Úspěšnost nalezení optimálního řešení je nižší mezi 70 a 80 procenty. Největší problém algoritmu působí sady Q a R označené za zavádějící, kde hodnoty vah proměnných algoritmus vedou k neplatným ohodnocením To je nejvíce patrné pro sadu wuf75-325, kde algoritmus dosahuje dobrého procenta při hledání ohodnocení splňující formuli avšak optima dosahuje málokdy. Algoritmus preferuje stavy, kde je formule splněna před stavy s vyšší vahou, ale zdá se že nedostatečně. Možným řešením může být úprava koeficientu ceny. Instance algoritmus zpracovává uspokojivě rychle.
\newpage
\section{Přílohy}
V přiloženém archivu jsou k nahlédnutí další grafy z whitebox testování algoritmu stejně tak jako všechny zdrojové kódy programu spolu s pomocnými skripty pro spouštění testů a zpracování dat.


\begin{thebibliography}{9}

\bibitem{zadani}
SCHMIDT, Jan. \textit{Experimentální vyhodnocení algoritmu}. Online. In: SCHMIDT, Jan. Courses.~2023. Dostupné z: \url{https://courses.fit.cvut.cz/NI-KOP/homeworks/files/task1.html}. [cit. 2023-12-27].

\bibitem{boost}
\textit{Boost}. Online. Boost. 2004. Dostupné z: \url{https://www.boost.org/}. [cit. 2024-02-10].

\bibitem{pred}
SCHMIDT, Jan. \textit{POKROČILÉ HEURISTIKY SIMULOVANÉ OCHLAZOVÁNÍ}. Online. In: SCHMIDT, Jan. Courses.~2023. Dostupné z: \url{https://courses.fit.cvut.cz/NI-KOP/lectures/files/2023/KOP08\%20Simulovan\%C3\%A9\%20ochlazov\%C3\%A1n\%C3\%AD.pdf}. [cit. 2024-02-10].

\bibitem{data}
SCHMIDT, Jan. \textit{Data a programy}. Online. In: SCHMIDT, Jan. Courses.~2023. Dostupné z: \url{https://courses.fit.cvut.cz/NI-KOP/download/index.html}. [cit. 2023-12-27].

\end{thebibliography}

\end{document}