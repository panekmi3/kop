#ifndef __CSTATE_H__
#define __CSTATE_H__

//#include <vector>
#include "boost/container/small_vector.hpp"
#include <cstdlib>
#include <cassert>
#include <memory>
#include "CInstance.hpp"
#include "constants.hpp"

using namespace boost::container;

class CState
{
private:
    small_vector<bool, 80> values;
    CInstance instance;

    int satisfied;
    int weightedSum;

    int calcSatisfied() const;
    int calcWeightedSum() const;
public:
    CState() = default;
    CState(CInstance instance, bool fill);
    CState(const CState & other) = default;
    CState( const small_vector<bool, 80> & v,const CInstance & i );
    CState( CInstance instance, size_t seed);
    ~CState() = default;
    bool operator > (const CState & other) const;
    
    bool solution() const;

    std::shared_ptr<CState> getRandomNeighbor() const;
    int getSatisfied() const {return satisfied;};
    double cost()  const;

    void printWhiteBox(std::ostream & out);

    friend std::ostream & operator << (std::ostream &out, const CState & state);
};

CState::CState( CInstance instance, bool fill)
{
    this->instance = instance;
    values.assign(instance.getVariables(), fill);

    satisfied = calcSatisfied();
    weightedSum = calcWeightedSum();
}
  
CState::CState( CInstance instance, size_t seed )
{
    srand(seed);
    this->instance = instance;
    for (size_t i = 0; i < instance.getVariables(); i++)
    {
        values.push_back(rand()%2);
    }

    satisfied = calcSatisfied();
    weightedSum = calcWeightedSum();
}

CState::CState(const small_vector<bool, 80> & v, const CInstance & i ) 
{
    this->instance = i;
    this->values = v;

    satisfied = calcSatisfied();
    weightedSum = calcWeightedSum();
}

int CState::calcWeightedSum() const
{
    int sum = 0;
    for (size_t i = 0; i < values.size(); i++)
        sum+= instance.getWeights()[i] * (int) values[i];
    return sum;
}

bool CState::solution() const
{
    return satisfied == ( (int) instance.getClauses().size() );
}

int CState::calcSatisfied() const
{
    int sum = 0;
    for (auto &&clause : instance.getClauses())
    {
        sum += (int) clause.eval(values);
    }
    return sum;
}

bool CState::operator > (const CState & other) const
{
    return this->cost() > other.cost();
}

std::shared_ptr<CState> CState::getRandomNeighbor() const
{
    small_vector<bool, 80> newValues = values;
    if(solution())
    {
        int index = rand() % newValues.size();
        newValues[index] = ! newValues[index]; 
    }
    else
    {
        small_vector<CClause, 325> unsat_clauses;
        for (auto &&clause : instance.getClauses())
        {
            if ( ! clause.eval(values))
            {
                unsat_clauses.push_back(clause);
            }
        } 

        assert(unsat_clauses.size());

        int index = rand() % unsat_clauses.size();
        CClause clause = unsat_clauses[index];
        small_vector<CLiteral, 3> literals = clause.getLiterals();
        index = rand() % literals.size();
        CLiteral literal = literals[index];
        newValues[literal.getIndex()] = ! newValues[literal.getIndex()];
    }
    return std::make_shared<CState>(newValues, instance);
}

double CState::cost() const
{
    double normalWeigt = (weightedSum * 1.0) / instance.getLargestWeight();
    double normalSat = ( satisfied * 1.0 ) / instance.getClauses().size();

    return CC * normalWeigt + (1-CC) * normalSat;
}

std::ostream & operator << (std::ostream &out, const CState & state)
{
    auto temp = splitSring( state.instance.name, '/' ).back();
    auto displayName = splitSring(temp, '.').front();

    out << displayName.substr(1) << " " << state.weightedSum << " ";
    
    for (size_t i = 0; i < state.values.size(); i++)
    {
        out << ( state.values[i] ? "":"-" ) << i+1 << " ";
    } 
    out << "0" << std::endl;

    return out;
}

void CState::printWhiteBox(std::ostream & out)
{
    out << satisfied << " " << weightedSum << " " << cost() << std::endl;
}


#endif // __CSTATE_H__