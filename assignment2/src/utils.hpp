#ifndef __UTILS_H__
#define __UTILS_H__

#include <vector>
#include <string>
#include <iostream>
#include <sstream>

std::vector<std::string> splitSring( std::string inputString, char delimeter)
{
    std::istringstream inputStream(inputString);
    std::vector<std::string> ret;
    std::string subString;
    while(std::getline( inputStream, subString, delimeter))
        ret.push_back(subString);
    
    return ret;
}

#endif // __UTILS_H__