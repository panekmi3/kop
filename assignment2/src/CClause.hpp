#ifndef __CCLAUSE_H__
#define __CCLAUSE_H__

#include <cstdlib>
//#include <vector>
#include <iostream>

#include "boost/container/small_vector.hpp"
#include "CLiteral.hpp"

class CClause
{
private:
    small_vector<CLiteral, 3> literals;
public:
    CClause(const small_vector<CLiteral, 3>& l);
    CClause(small_vector<CLiteral, 3>&& l);
    bool eval(const small_vector<bool, 80> & values) const;

    small_vector<CLiteral, 3> getLiterals() const {return literals;};

    friend std::ostream & operator << (std::ostream &out, const CClause & clause);
};

CClause::CClause(const small_vector<CLiteral, 3>& l)
{
    this->literals = l;
}

CClause::CClause(small_vector<CLiteral, 3>&& l)
{
    this->literals = std::move(l);
}

bool CClause::eval( const small_vector<bool, 80> & values) const
{
    for (size_t i = 0; i < literals.size(); i++)
    {
        if ( literals[i].eval(values)) return true;
    }
    return false;
}

std::ostream & operator << (std::ostream &out, const CClause & clause)
{
    for (auto &&l : clause.literals)
    {
        out << l << ", ";
    }
    out << std::endl;
    return out;
}

#endif // __CCLAUSE_H__