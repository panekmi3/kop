#include <iostream>
#include <vector>
#include <random>
#include <iomanip>
#include <memory>

#include "CInstance.hpp"
#include "CState.hpp"
#include "constants.hpp"

using namespace std;

std::uniform_real_distribution<double> unif(0.0,1.0);
std::default_random_engine re;

shared_ptr<CState> try_state( shared_ptr<CState> state, double temperature)
{
    shared_ptr<CState> neighbor = state->getRandomNeighbor();
    if( neighbor->cost() > state->cost() ) return neighbor;

    double diff = state->cost() - neighbor->cost();

    if( unif(re) < exp( -diff/temperature) )
        return neighbor;
    return state;
}

double cool( double temperature)
{
    return temperature * CR;
}

int main(int argc, char const *argv[])
{

    CInstance instance(argv[1]);

    srand((int) time(NULL) );
    cout << fixed;
    cout << setprecision(5);
    cerr << fixed;
    cerr << setprecision(5);

    shared_ptr<CState> bestState, bestSolved;
    shared_ptr<CState> state = make_shared<CState>( CState( instance, (size_t)rand()));

    int numberOfClauses = instance.getClauses().size();

    double temperature = IT;
    double endTemp = ET;
    int equilibrium = EQ;
    bestState = make_shared<CState>( CState(instance, false));
    bestSolved = make_shared<CState>( CState(instance, false) );
    
    #ifdef WHITEBOX
        cerr << instance.name << " " << instance.getClauses().size() << " " << EQ << " " << IT << " " << ET << " " << CR << " " << CC << endl;
    #endif

    while ( temperature > endTemp )
    {
        while(equilibrium)
        {
            state = try_state(state, temperature);

            if( state->cost() > bestState->cost() )
            {
                bestState = state;
                if(state->solution())
                    bestSolved = state;             
            }

            equilibrium--;        
        }
        equilibrium = EQ;
        temperature = cool(temperature);
        
    }

    if ( ! bestSolved->solution()) bestSolved = bestState;

    cout << * bestSolved;
    cerr << bestSolved->getSatisfied() << endl;
    return 0;
}
