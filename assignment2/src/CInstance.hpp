#ifndef __CINSTANCE_H__
#define __CINSTANCE_H__

#include <vector>
#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <string>

#include "utils.hpp"
#include "CClause.hpp"

class CInstance
{
private:
    size_t variables;
    std::vector<int> weights;
    std::vector<CClause> clauses;
    int largestWeight;
public:
    std::string name; 
    CInstance() = default;
    CInstance(const CInstance & other) = default;
    CInstance(std::string filename);
    ~CInstance() = default;
    size_t getVariables() const {return variables;}
    std::vector<int> getWeights() const {return weights;}
    std::vector<CClause> getClauses() const {return clauses;}
    int getLargestWeight() const {return largestWeight;};

    friend std::ostream & operator << (std::ostream &out, const CInstance & inctance);
};

CInstance::CInstance( std::string filename )
{
    std::ifstream instance_file(filename);
    if ( ! instance_file.is_open())
        throw;
    
    
    std::string line;
    
    while (getline(instance_file, line))
    {
        auto first = line[0];
        if(first == 'c') continue; //line is a comment, ignore
        if(first == 'p')
        {
            auto line_split = splitSring(line, ' ');
            variables = atoi(line_split[2].c_str());
        }
        else if(first == 'w')
        {
            largestWeight = 0;
            auto line_split = splitSring(line, ' ');
            weights.reserve(variables);
            for (size_t i = 0; i < variables; i++)
            {   
                int weight = atoi(line_split[i+1].c_str());
                weights.push_back( weight );
                if(weight > largestWeight) largestWeight = weight;
            }
        }
        else
        {
            auto line_split = splitSring(line, ' ');
            if (line_split[0][0]==0) line_split.erase(line_split.begin());//there is an extra space in instance files for some reason
            small_vector<CLiteral, 3> literals;
            for (size_t i = 0; i < line_split.size() - 1; i++)
            {
                int value = atoi(line_split[i].c_str());
                literals.push_back( CLiteral( abs(value), value > 0 ));
            }
            clauses.push_back(CClause(std::move(literals)));
        }
        
    }

    instance_file.close();
    this->name = filename;
 }

std::ostream & operator << (std::ostream &out, const CInstance & instance)
{
    out << "Variables:" << instance.variables << std::endl;
    out << "Weights:" << instance.largestWeight <<  std::endl;
    for (auto &&w : instance.weights)
    {
        out << w << ", ";
    } out << std::endl;

    out << "Clauses: " << instance.clauses.size() << std::endl;
    for (auto &&c : instance.clauses)
    {
        out << c;
    }
    return out;
}


#endif // __CINSTANCE_H__