#ifndef __CLITERAL_H__
#define __CLITERAL_H__

#include <cstdlib>
//#include <vector>
#include "boost/container/small_vector.hpp"
#include <iostream>

using namespace boost::container;

class CLiteral
{
private:
    size_t index;
    bool neg;
public:
    CLiteral(size_t i, bool d);
    bool eval(const small_vector<bool, 80> & values) const;
    size_t getIndex() const {return index;};

    friend std::ostream & operator << (std::ostream &out, const CLiteral & clause);
};

CLiteral::CLiteral(size_t i, bool d)
{
    this->index = i-1;
    this->neg = ! d;
}

bool CLiteral::eval( const small_vector<bool, 80> & values ) const
{
    bool value = values[this->index];

    if (neg) return !value;
    return value;
}

std::ostream & operator << (std::ostream &out, const CLiteral & lit)
{
    out << ( lit.neg ? "-":"" ) << lit.index;
    return out;
}


#endif // __CLITERAL_H__